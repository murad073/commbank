﻿using System.Collections.Generic;
using System.Linq;
using CommBank.Common;
using MoviesLibrary;

namespace CommBank.MovieData.MoviesLibrary
{
    public class MovieDataAdapter : IMovieData
    {
        private readonly MovieDataSource _movieDataSource = new MovieDataSource();
        public Movie GetDataById(int id)
        {
            return GetMovieFromMovieData(_movieDataSource.GetDataById(id));
        }

        public List<Movie> GetAllData()
        {
            return _movieDataSource.GetAllData().Select(GetMovieFromMovieData).ToList();
        }

        public void Update(Movie movie)
        {
            _movieDataSource.Update(GetMovieDataFromMovie(movie));
        }

        public int Create(Movie movie)
        {
            return _movieDataSource.Create(GetMovieDataFromMovie(movie));
        }

        private Movie GetMovieFromMovieData(global::MoviesLibrary.MovieData movieData)
        {
            return new Movie()
            {
                Cast = movieData.Cast,
                Classification = movieData.Classification,
                Genre = movieData.Genre,
                MovieId = movieData.MovieId,
                Rating = movieData.Rating,
                ReleaseDate = movieData.ReleaseDate,
                Title = movieData.Title
            };
        }

        private global::MoviesLibrary.MovieData GetMovieDataFromMovie(Movie movie)
        {
            return new global::MoviesLibrary.MovieData()
            {
                Cast = movie.Cast,
                Classification = movie.Classification,
                Genre = movie.Genre,
                MovieId = movie.MovieId,
                Rating = movie.Rating,
                ReleaseDate = movie.ReleaseDate,
                Title = movie.Title
            };
        }
    }
}

