﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommBank.Common;

namespace CommBank.MovieData.MoviesLibrary
{
    public static class MovieDataFactory
    {
        public static IMovieData CreateDefaultMovieData()
        {
            return new MovieDataAdapter();
        }
    }
}

