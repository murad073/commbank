﻿using MoviesLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommBank;
using CommBank.Controllers;
using Microsoft.Practices.Unity;
using CommBank.Models;
using CommBank.Tests.Stubs;
using FizzWare.NBuilder;

namespace CommBank.Tests.Controllers
{
    [TestClass]
    public class ValuesControllerTest
    {
        public static UnityContainer container;

        //Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            container = new UnityContainer();
            container.RegisterType<IExtendedMovieDataSource, SearchableMovieData>(new HierarchicalLifetimeManager());
            container.RegisterType<IMovieDataSource, MovieDataSourceStub>(new HierarchicalLifetimeManager());
        }

        [TestMethod]
        public void Get()
        {
            // Arrange
            ValuesController controller = container.Resolve<ValuesController>();

            // Act
            List<MovieData> result = controller.Get();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(10, result.Count());
            Assert.AreEqual(1, result.ElementAt(0).MovieId);
            Assert.AreEqual(10, result.ElementAt(9).MovieId);
        }

        [TestMethod]
        public void GetById()
        {
            // Arrange
            ValuesController controller = container.Resolve<ValuesController>();

            // Act
            MovieData result = controller.Get(5);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(5, result.MovieId);
            Assert.AreEqual("Title5", result.Title);
            Assert.AreEqual("Classification5", result.Classification);
            Assert.AreEqual("Genre5", result.Genre);
            Assert.AreEqual(5, result.Rating);
            Assert.AreEqual(5, result.ReleaseDate);
        }

        [TestMethod]
        public void Post()
        {
            // Arrange
            ValuesController controller = container.Resolve<ValuesController>();

            // Act
            int NewId = controller.Post(Builder<MovieData>.CreateNew().Build());

            // Assert
            Assert.AreEqual(11, controller.CachedExtendedMovieDataSource.GetAllData().Count());
            Assert.AreEqual(11, NewId);
        }

        //[TestMethod]
        //public void Put()
        //{
        //    // Arrange
        //    ValuesController controller = new ValuesController();

        //    // Act
        //    controller.Put(5, "value");

        //    // Assert
        //}
    }
}
