﻿using CommBank.Models;
using FizzWare.NBuilder;
using MoviesLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommBank.Tests.Stubs
{
    class MovieDataSourceStub : IMovieDataSource
    {
        private List<MovieData> FakeMovieDataList;
        public MovieDataSourceStub()
        {
            FakeMovieDataList = Builder<MovieData>.CreateListOfSize(10).All().Build().ToList();
        }
        public MovieData GetDataById(int id)
        {
            return FakeMovieDataList.Where(x => x.MovieId == id).FirstOrDefault();
        }
        public List<MovieData> GetAllData()
        {
            return FakeMovieDataList.OrderBy(x => x.MovieId).ToList();
        }
        public void Update(MovieData md)
        {
            FakeMovieDataList.RemoveAll(x => x.MovieId == md.MovieId);
            FakeMovieDataList.Add(md);
        }
        public int Create(MovieData md)
        {
            int NewId = FakeMovieDataList.Max(x => x.MovieId) + 1;
            md.MovieId = NewId;
            FakeMovieDataList.Add(md);
            return NewId;
        }
    }
}
