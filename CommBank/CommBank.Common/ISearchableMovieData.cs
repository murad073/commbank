﻿using System.Collections.Generic;

namespace CommBank.Common
{
    public interface ISearchableMovieData : IMovieData
    {
        List<Movie> GetAllData(string orderBy, string sortDirection);
        List<Movie> Search(string searchText);
    }
}
