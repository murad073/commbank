﻿using System.Collections.Generic;

namespace CommBank.Common
{
    public interface IMovieData
    {
        Movie GetDataById(int id);
        List<Movie> GetAllData();
        void Update(Movie movie);
        int Create(Movie movie);
    }
}

 