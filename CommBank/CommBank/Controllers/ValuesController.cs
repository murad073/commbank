﻿using MoviesLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Runtime.Caching;
using CommBank.Models;

namespace CommBank.Controllers
{
    public class ValuesController : ApiController
    {
        public ValuesController(IExtendedMovieDataSource emds)
        {
            CachedExtendedMovieDataSource = emds;
        }

        public IExtendedMovieDataSource CachedExtendedMovieDataSource
        {
            get
            {
                //ObjectCache is thread safe, no need to lock it
                //If the data exists in memory cache, pull it from there, otherwise make a call to the data source to get the data
                ObjectCache cache = MemoryCache.Default;
                return cache.Get("CachedMovieDataSource") as SearchableMovieData;
            }
            set
            {
                ObjectCache cache = MemoryCache.Default;
                if (cache.Get("CachedMovieDataSource") == null)
                {
                    CacheItemPolicy policy = new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.Now.AddYears(1) };
                    cache.Add("CachedMovieDataSource", value, policy);
                }
            }
        }

        /// <summary>
        /// GET api/Movie/5. Get a movie by id. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/Movie/Get/{id}")]
        public MovieData Get(int id)
        {
            try
            { return CachedExtendedMovieDataSource.GetDataById(id); }
            catch (Exception)
            { throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)); }
        }

        /// <summary>
        /// GET api/values. Fetch all the movies from the third party datasource. 
        /// </summary>
        /// <returns></returns>
        [Route("api/Movie/Get")]
        public List<MovieData> Get()
        {
            try
            { return CachedExtendedMovieDataSource.GetAllData(); }
            catch (Exception)
            { throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)); }
        }

        /// <summary>
        /// Return movies in a sorted order by any of the movie attributes. (Except for the field “Cast”)
        /// Can be in ascending or descending order
        /// </summary>
        /// <param name="order_by"></param>
        /// <param name="sort_direction"></param>
        /// <returns></returns>
        [Route("api/Movie/Get/{order_by}/{sort_direction}")]
        public List<MovieData> Get(string order_by, string sort_direction)
        {
            try
            {
                return CachedExtendedMovieDataSource.GetAllData(order_by, sort_direction);
            }
            catch (SearchableMovieData.WrongArgumentException)
            { throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)); }
            catch (Exception)
            { throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)); }
        }

        /// <summary>
        /// Search across all fields within the movies list and return the matching occurences.
        /// </summary>
        /// <param name="search_text"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/Movie/Search/{search_text}")]
        public List<MovieData> Search(string search_text)
        {
            try
            { return CachedExtendedMovieDataSource.Search(search_text.ToLower()); }
            catch (Exception)
            { throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)); }
        }

        /// <summary>
        /// PUT api/Movie/5. Update an existing movie. 
        /// </summary>
        /// <param name="value"></param>
        [Route("api/Movie/Put")]
        public void Put([FromBody]MovieData value)
        {
            try
            { CachedExtendedMovieDataSource.Update(value); }
            catch (Exception)
            { throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)); }
        }

        /// <summary>
        /// POST api/values. Insert a new movie. 
        /// </summary>
        /// <param name="value"></param>
        [Route("api/Movie/Post")]
        public int Post([FromBody]MovieData value)
        {
            try
            { return CachedExtendedMovieDataSource.Create(value); }
            catch (Exception)
            { throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)); }
        }
    }
}

