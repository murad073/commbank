﻿using CommBank.Models;
using Microsoft.Practices.Unity;
using System.Web.Http;
using CommBank.Resolver;

namespace CommBank
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            var container = new UnityContainer();
            container.RegisterType<IExtendedMovieDataSource, SearchableMovieData>(new HierarchicalLifetimeManager());
            container.RegisterType<IMovieDataSource, iMovieDataSource>(new HierarchicalLifetimeManager());
            config.DependencyResolver = new UnityResolver(container);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
