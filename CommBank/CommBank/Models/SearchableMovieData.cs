﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using CommBank.Common;
using CommBank.MovieData.MoviesLibrary;

namespace CommBank.Models
{
    public class SearchableMovieData : ISearchableMovieData
    {
        private readonly IMovieData _movieData = MovieDataFactory.CreateDefaultMovieData();

        public List<Movie> GetAllData(string orderBy, string sortDirection)
        {
            if (typeof(Movie).GetProperty(orderBy) == null || orderBy == "Cast")
            { throw new WrongArgumentException(); }

            // Using Dynamic LINQ for the OrderBy
            return GetAllData().OrderBy(orderBy + ((sortDirection.ToLower().Contains("desc")) ? " DESC" : " ASC")).ToList();
        }

        public List<Movie> Search(string searchText)
        {
            return GetAllData().Where(x => x.Title.ToLower().Contains(searchText)
                || x.Genre.ToLower().Contains(searchText)
                || x.Classification.ToLower().Contains(searchText)
                || x.Rating.ToString().Contains(searchText)
                || x.ReleaseDate.ToString().Contains(searchText)
                || string.Join(string.Empty, x.Cast).ToLower().Contains(searchText)
                ).ToList();
        }

        public Movie GetDataById(int id)
        {
            return _movieData.GetDataById(id);
        }

        public List<Movie> GetAllData()
        {
            return _movieData.GetAllData();
        }

        public void Update(Movie movie)
        {
            _movieData.Update(movie);
        }

        public int Create(Movie movie)
        {
            return _movieData.Create(movie);
        }

        public sealed class WrongArgumentException : Exception
        { }
    }
}
